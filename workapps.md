# Overview

I was recently looking for a replacement for Remember the Milk. RTM has been my main task manager for a while now (I pay $40/year for the Pro version) but it just doesn't cut it any longer. It's been several years since I've seriously looked at the options in this area, and wow, was I surprised at the number of available options. Here's a list of some of the many options I came across with a minimal amount of searching. It's hard to imagine all of these apps still being in existence five years from now (January 2019).

I didn't intend for this list to get this long, but so many apps come up with a simple search that I just kept adding to it. After a while it started to amaze me at just how many apps I was finding.

# Open Source

Osmo is an app for Linux. It provides a calendar, task manager, notes, and contacts. Osmo exceeded my (admittedly low) expectations. Something I really like is that everything is stored in plain text, meaning you can easily put it under version control. This is a lightweight, low-overhead option that will work well if you don't have too much stuff to track.

Getting Things GNOME! is a lightweight task manager.

Gnome to do is another lightweight task manager.

OpenProject is easy to get running with Docker. It does much more than task management.

Nextcloud includes a task manager, calendar, and file syncing, among other things.

Kanboard is an open source Trello replacement.

KOrganizer

Humhub

Goodwork

web2project

Tuleap

Crepido

Restyaboard

Wekan

# Online

Todoist has a free plan, but it's largely useless - almost all the good functionality like reminders and file attachments require a Premium or Business plan. If you're an educator, you can get the Business plan for 70% off, or $18/year.

Toodledo is packed with functionality, with the lowest cost paid plan only $36. I don't care much for the design of the interface (which cannot be customized). Even worse, it's not clear how easy it is to cancel a paid plan. The company has been asked in the forum, and there is total silence on their part, a strange outcome given how little it would take to make clear how to cancel. I don't have any desire to mess with a company that plays games with its customers.

Asana is free for individuals and small teams.

Freedcamp does a lot of the same things as Basecamp, but single users can get a paid plan for as low as $18/year.

Agantty is free. Not sure if they sell your data or what's going on there.

Yodiz is free for teams of up to three people. It's $36/year for the cheapest paid plan. Looks like it's primarily for software development.

Bitrix24 is free for teams of 12 or fewer users. Comes with 5 GB of data. I've never used it, but the free plan should be more than sufficient for single users.

paymo offers a free plan for single users.

Quire is free for now - not sure that's a good thing.

notion.so is $48/year

any.do is $36/year for premium. I haven't used the app and don't know exactly what it does.

Trello is free but I just don't like the interface.

Flock is $54/year.

Taskulu

Glip offers a free plan that will probably suffice for most users.

Zoho is $12/year. I'm not sure how much you get for your money.

allthings is $48/year.

Podio offers free task management. Full service starts at $86/year.

Wimi offers 8 projects for free. Paid starts at $9/user/month.

Remember The Milk is $40/year.

Moovia is free for two members.

ClickUp starts at $5/user/month.

Flow starts at $57/user/year.

Zenkit has a reasonable free plan. $9/user/month after that.

Ora is free for up to three users.

Centrallo has a free plan and premium is $5/month.

Checkvist is $39/year.

I Done This starts at $48/year.

SomTodo is $37.40/year.

easynote.io is free and has paid plans starting at $60/year.

Pintask is free with paid extensions. Like Trello but offers email reminders for free.

TrackingTime is $50/year.

# Higher price options

Ryver used to be free (when it was in beta status, with an ugly user experience). Now they don't even have a free plan. The cheapest plan starts at $49/month. They make an odd argument about how it's cheaper than Slack+Trello, but Trello is free for most uses, and the paid Slack plan is cheaper for team of seven or fewer. That's a bizarre strategy, because they're making it really expensive to start out.

Basecamp is a great deal for large organizations at $99/month. It does everything you need, but that's too expensive for one person.

Airtable lets you create databases of any kind. The free version is probably sufficient for most people. Paid plans start at $120/year.

Teamwork Projects is $9/user/month (maybe not a bad deal depending on what you're doing). Teamwork Chat is free.

Smartsheet starts at $14/user/month.

Jira starts at $10/month.

Proofhub is $89/month for the regular version or $45/month for the lite version.

Sprintground starts at 24 euros/month.

Meistertask starts at $8.25/user/month.

Quip starts at $30/month.

Taskworld is $11/user/month.

Casual starts at $84/year.

Trackolade starts at $19/month.

Redbooth starts at $9/user/month.

Nozbe starts at $96/year.

Hive starts at $12/user/month.

monday.com starts at $300/year.

Wrike starts at $9.80/user/month.

ActiveCollab starts at $75/year.

Mavenlink starts at $19/month.

ProWorkflow starts at $10/user/month.

Worksection starts at $29/month.

Copper Project starts at $29/month.

Project Manager starts at $15/user/month.

Scoro starts at $26/user/month.

FunctionFox starts at $35/month.

LiquidPlanner starts at $45/user/month.

Proggio starts at $8/user/month.

HiTask is $5/user/month.

Planiro is $5/user/month.

Appigo todo is free or premium starts at $20/year.

Accelo starts at $16/user/month.

Workamajig starts at $50/user/month.

Jolt starts as $80/month.

RuggedTask is $84/year.

Focuster starts at $96/year.

Swipes is $72/year.

# Software with weird sites

Workflowy doesn't provide any information.

Cozi doesn't provide any information about the price.

Huddle doesn't give a price.

Unison's website doesn't come up - maybe they are no longer in business.

Workzone doesn't provide prices.

Clarizen doesn't provide prices.

TeuxDeux doesn't provide prices.