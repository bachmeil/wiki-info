# Wikis

There are many wikis out there. I've been looking for the perfect wiki solution for a few years now. Here's an opinionated list of the wikis.
As always, YMMV.

# Things to consider

## Database vs flat files

Some wikis require a database. That requires *a lot* of additional work if, like me, you don't often work with databases.
A database adds a lot of overhead: additional steps during setup, additional security concerns, the need to regularly back it up (you
can't just put it in your version controlled repo), and hassle if you want to move/upgrade your wiki.

The alternative is to store all the data in flat files (text files). This works well if you use version control like Git or Fossil.
You get backup for free, there's no additional setup or security involved, and moving/upgrading the wiki requires copying files.

This is the first question you have to ask yourself. I would personally never consider a wiki that requires a database due to
the crazy amount of overhead and maintenance that comes with. Unless you're going to have an extremely large wiki, performance will be
acceptable with flat files, and might actually be better than with a database. Here's [a discussion](http://www.pmwiki.org/wiki/PmWiki/FlatFileAdvantages)
of the decision to use flat files for PmWiki.

## Personal vs public wiki

The days of truly public wikis are behind us. You need to decide if you want a personal wiki, meaning one that runs on your local computer, or
a wiki that you run on a web server, and that potentially others could see.

A personal wiki eliminates any concerns about security. It allows you to write anything you want without worrying about the
consequences. You will run a local web server and view the pages in your browser. You can share a personal wiki with others
using Dropbox or Git.

A public wiki is more convenient. You can access it from anywhere, and add content from a mobile device. It's also a big hassle because
you have to keep it secure.

## Functionality and customizability

You should know in advance what you plan to do with your wiki. If you just want a way to archive knowledge (say, related to a
project you're working on) any wiki will do. Some wikis offer a greater amount of functionality than others. You may have to
use a plugin system to add functionality. If you're a programmer, the ease with which you can add your own functionality may
also be a consideration.

## Default theme

I'd warn against judging a wiki based on the default theme. First, the theme isn't as important as the ability to capture the
information you need. Second, just about all wikis will allow you to customize the theme with a CSS file.

# The List

Here we go. These are the wikis I'd recommend looking at. They appear to have enough activity to be realistic choices.

**PmWiki** [site](http://www.pmwiki.org/) Flat files. Written in PHP. Can be run locally using a PHP server: `php -S localhost:8001`. Then open your 
browser to localhost:8001/pmwiki.php. That's about it.

**Dokuwiki** [site](https://www.dokuwiki.org/dokuwiki#) Same comments as PmWiki apply. I have found Dokuwiki to have more
security-related overhead than PmWiki at setup.

**Oddmuse** [site](https://oddmuse.org/wiki) Flat files. Written in Perl. AFAICT, no way to use this as a personal wiki.

**ikiwiki** [site](https://ikiwiki.info/) This is a wiki compiler - see the documentation. Designed for use inside a version
controlled repo. The default theme is kind of plain, as is the website, but that's a trivial change.

**Moinmoin** [site](https://moinmo.in/) Flat files. Written in Python. Works well as a personal wiki. Used for the Ubuntu wiki.

**gitit** [site](https://github.com/jgm/gitit) Flat files. Intended to be used inside a Git repo. Can export pages to many 
different formats. Written by the creator of Pandoc.

**Tiki** [site](https://tiki.org/tiki-index.php) Heavier on the setup and maintenance side than the others listed here,
but offers many features, including forums, calendars, polls, and even a spreadsheet. This is the "kitchen sink" wiki. I can't
imagine anyone using it as a personal wiki.

**Fossil** [site](http://www.fossil-scm.org/index.html/doc/trunk/www/index.wiki) This isn't really just a wiki. It's a
version control system, but it has a wiki built into it, and you can easily make it available to the public. The sqlite
website is a Fossil wiki.

**TiddlyWiki** [site](https://tiddlywiki.com/) An interesting approach - it's a single html file. Changes are embedded into
the html and saved to disk.